/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.config.security;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class RolesTest {
	
	public interface TestInterface {
		static String TEST_FIELD_1 = "TEST_VALUE_1";
		static String TEST_FIELD_2 = "TEST_VALUE_2";
		static String TEST_FIELD_3 = "TEST_VALUE_3";
	}

	@Test
	public void testStaticStringFieldsValueCollection() {
		List<String> result = Roles.StringFieldsCollector.collect(TestInterface.class);
		assertEquals(3, result.size());
		assertTrue(result.contains("TEST_VALUE_1"));
		assertTrue(result.contains("TEST_VALUE_2"));
		assertTrue(result.contains("TEST_VALUE_3"));
	}
	
}

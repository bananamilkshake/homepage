DROP TABLE IF EXISTS projects;
DROP TABLE IF EXISTS information;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS article_tags;
DROP TABLE IF EXISTS articles;
DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS principals;
DROP TABLE IF EXISTS databasechangelog;
DROP TABLE IF EXISTS databasechangeloglock;

DROP SEQUENCE IF EXISTS articles_id_seq;
DROP SEQUENCE IF EXISTS comments_id_seq;
DROP SEQUENCE IF EXISTS principals_id_seq;
DROP SEQUENCE IF EXISTS projects_id_seq;

-- Create admin user
INSERT INTO PRINCIPALS(ID, USERNAME, PASSWORD) VALUES(1, 'admin', '$2a$10$rTN31w8pYP9oHjwGFYPtMeC1XeUqNj2VuSvjR.9VahoMauKm67HqS');
INSERT INTO AUTHORITIES(PRINCIPAL_ID, ROLE) VALUES(1, 'ROLE_ADMIN');

-- Create some articles
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (1, true, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '1: Lorem Ipsum', '2012-01-11 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (2, true, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2: Lorem Ipsum', '2012-01-12 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (3, true, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '3: Lorem Ipsum', '2012-02-01 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (4, false, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '4: Lorem Ipsum', '2012-03-07 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (5, false, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '5: Lorem Ipsum', '2012-03-08 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (6, false, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '6: Lorem Ipsum', '2012-03-09 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (7, false, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '7: Lorem Ipsum', '2012-03-10 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (8, true, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '8: Lorem Ipsum', '2012-03-11 11:12:13');
INSERT INTO ARTICLES(ID, PUBLISHED, TEXT, TITLE, CREATION_DATE) VALUES (9, true, '<p><i>This is italic</i></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '9: Lorem Ipsum', '2012-03-12 11:12:13');

INSERT INTO ARTICLE_TAGS(ARTICLE_ID, TAGS) VALUES (8, 'Linux');
INSERT INTO ARTICLE_TAGS(ARTICLE_ID, TAGS) VALUES (9, 'Linux');
INSERT INTO ARTICLE_TAGS(ARTICLE_ID, TAGS) VALUES (9, 'C++');
INSERT INTO ARTICLE_TAGS(ARTICLE_ID, TAGS) VALUES (9, 'Fun');
INSERT INTO ARTICLE_TAGS(ARTICLE_ID, TAGS) VALUES (9, 'Some tag');

INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL, PRINCIPAL_ID) VALUES(1, 'RootComment', 'Comment 1', '2012-03-12 11:12:13', 9, NULL, 'admin', 'zi@mail.com', 1);
INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL, PRINCIPAL_ID) VALUES(2, 'AnswerComment', 'Comment 2 (answer to 1)', '2012-03-12 11:12:13', NULL, 1, 'admin', 'zi@mail.com', 1);
INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL) VALUES(3, 'AnswerComment', 'Comment 3 (answer to 2)', '2012-03-12 11:12:13', NULL, 2, 'admin', 'zi@mail.com');
INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL) VALUES(4, 'RootComment', 'Comment 4', '2012-03-12 11:12:13', 9, NULL, 'admin', 'zi@mail.com');
INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL) VALUES(5, 'AnswerComment', 'Comment 5 (answer to 4)', '2012-03-12 11:12:13', NULL, 4, 'admin', 'zi@mail.com');
INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL) VALUES(6, 'AnswerComment', 'Comment 6 (answer to 4)', '2012-03-12 11:12:13', NULL, 4, 'admin', 'zi@mail.com');
INSERT INTO COMMENTS(ID, TYPE, TEXT, CREATION_DATE, ARTICLE_ID, ANSWERTO_ID, CREATOR_NAME, EMAIL) VALUES(7, 'RootComment', 'Comment 7', '2012-03-12 11:12:13', 9, NULL, 'admin', 'zi@mail.com');

INSERT INTO PROJECTS(NAME, DESCRIPTION) VALUES('GeoTasks', 'Android application to create tasks that must be notified on some location.');
INSERT INTO PROJECTS(NAME, DESCRIPTION) VALUES('GWT phonebook', 'Google Web Toolkit (GWT) based web application uses for phonebook creation. You can add, edit, remove, delete, search and print list of all records. Project was developed as test task for Java developer vacancy.');
INSERT INTO PROJECTS(NAME, DESCRIPTION) VALUES('Level editor', 'Qt application to create simple levels for 2D platformer. Levels can be created, loaded and saved (you can find level example in downloads). Qt 5.4 version used. Project was developed as test task for C++ developer vacancy.');

INSERT INTO INFORMATION(ID, FULL_INFO, SHORT_INFO, NAME, EMAIL, LINKEDIN_LINK, BITBUCKET_LINK) VALUES (
	1,

	'<h2 class="ui header">My description :D</h2>
	<p>Hello! This info came from database!.</p>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
		labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
		laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
		voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
		cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',

	'C++ game developer at work, JavaEE programmer at home. Write code for fun. Currently live in Saint-Petersburg, Russia.',

	'bananamilkshake !!!',

	'me@bananamilkshake.me',

	'linkedin.com',

	'bitbucket.org'
);
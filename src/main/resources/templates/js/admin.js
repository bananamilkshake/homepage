/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

function showPreview(text, preview) {
	preview.html(text.val());
	text.hide();
	preview.show();
}

function hidePreview(text, preview) {
	text.show();
	preview.hide();
}

function setupPreviewToggle(buttonSelector, textSelector, previewSelector) {
	var text = $(textSelector);
	var preview = $(previewSelector);

	$(buttonSelector).click(function() {
		if ($(this).hasClass('js-preview')) {
			hidePreview(text, preview, $(this));
			$(this)
				.removeClass('js-preview')
				.text("Preview");
		} else {
			showPreview(text, preview, $(this));
			$(this)
				.addClass('js-preview')
				.text("Edit");
		}
	});
}

function showModal(title, onAccept) {
	$('.ui.modal > .header').text(title);
	$('.ui.modal')
		.modal({
			closable: false,
			onApprove: function() {
				var values = $('.ui.form')
					.form('get values');
				onAccept(values);
			}
		})
		.modal('show');
}

function showAddModal(title) {
	var _csrf = $('.ui.modal > .ui.form').form('get value', '_csrf');
	$('.ui.modal > .ui.form')
		.form('clear');
	$('.ui.modal > .ui.form').form('set value', '_csrf', _csrf);
	
	showModal(title, function(values) {
		$.post('add', values)
			.done(function(data) {
				location.reload(); // recreate page to see new article
			})
			.fail(function(jqXHR, textStatus) {
				displayError(textStatus);
			});
	});
}

function showEditModal(title, objectJson, elementId) {
	var object = JSON.parse(objectJson);
	$('.ui.modal > .ui.form')
		.form('set values', object);

	showModal(title, function(values) {
		$.post('edit', values)
			.done(function(data) {
				// Update element values
				$('#' + elementId).replaceWith(data);
			})
			.fail(function(jqXHR, textStatus) {
				displayError(textStatus);
			});
	});
}

function displayError(message) {
	$('#message-error')
		.removeClass('hidden')
		.find('.js-content')
			.text(message);
}

$(function() {
	$('.ui.dropdown')
	  .dropdown({
		allowAdditions: true
	  });

	$('.message .close')
		.on('click', function() {
			$(this)
				.closest('.message')
				.transition('fade');
		});
});
package bananamilkshake.homepage.jpa.repository;

import bananamilkshake.homepage.jpa.entity.InformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InformationRepository extends JpaRepository<InformationEntity, Long> {
}

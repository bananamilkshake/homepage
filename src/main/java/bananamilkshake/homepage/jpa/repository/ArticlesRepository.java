/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.jpa.repository;

import bananamilkshake.homepage.jpa.entity.ArticleEntity;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticlesRepository extends JpaRepository<ArticleEntity, Long>, PagingAndSortingRepository<ArticleEntity, Long> {
	List<ArticleEntity> findByPublishedTrueOrderByCreationDateDesc();

	List<ArticleEntity> findByPublishedTrue(Pageable pageRequest);
	
	ArticleEntity findOneByIdAndPublishedTrue(Long id);

	List<ArticleEntity> findByTagsAndPublishedTrue(String tag, Pageable pageRequest);
}

package bananamilkshake.homepage.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

@Converter(autoApply = true)
public class DateAttributeConverter implements AttributeConverter<ZonedDateTime, Date> {

	@Override
	public Date convertToDatabaseColumn(ZonedDateTime attribute) {
		Instant instant = Instant.from(attribute);
		return Date.from(instant);
	}

	@Override
	public ZonedDateTime convertToEntityAttribute(Date dbData) {
		Instant instant = dbData.toInstant();
		return ZonedDateTime.from(instant);
	}
}

/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.jpa.entity;

import bananamilkshake.homepage.model.Principal;
import java.util.List;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(
	name = "principals",
	uniqueConstraints = 
		@UniqueConstraint(columnNames = "username"))
@Access(AccessType.PROPERTY)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PrincipalEntity extends Principal {

	private String password;

	@Id
	@NotNull
	@Override
	public String getUsername() {
		return super.getUsername();
	}
	
	@NotNull
	public String getPassword() {
		return password;
	}
	
	@Override
	@Column(name="role")
	@CollectionTable(name = "authorities", joinColumns = @JoinColumn(name = "principal_name"))
	@ElementCollection(fetch = FetchType.EAGER)
	public Set<String> getAuthorities() {
		return super.getAuthorities();
	}

	public PrincipalEntity(String username, Set<String> authorities, String password) {
		super(username, authorities);
		this.setPassword(password);
	}

	public boolean equals(PrincipalEntity other) {
		return other != null && getUsername().equals(other.getUsername()) && getAuthorities().equals(other.getAuthorities());
	}
}

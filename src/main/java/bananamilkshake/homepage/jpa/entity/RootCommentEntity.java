/*
 * Copyright (C) 2016 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package bananamilkshake.homepage.jpa.entity;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@Access(AccessType.PROPERTY)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@DiscriminatorValue("RootComment")
public class RootCommentEntity extends CommentEntity {
	
	private ArticleEntity article;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	public ArticleEntity getArticle() {
		return article;
	}
	
	public RootCommentEntity(ArticleEntity article, PrincipalEntity creator, String creatorName, String email, String text, ZonedDateTime date) {
		super(creator, creatorName, email, text, date);
		this.setArticle(article);
	}
}

 /*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.jpa.entity;

import bananamilkshake.homepage.model.Project;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Table(
	name = "projects",
	uniqueConstraints =
		@UniqueConstraint(columnNames = "name")
)
@Access(AccessType.PROPERTY)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectEntity extends Project {

	@Id
	@Column
	@Override
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "projects_seq_gen")
	@SequenceGenerator(name = "projects_seq_gen", sequenceName = "projects_id_seq", allocationSize = 1)
	public Long getId() {
		return super.getId();
	}
	
	@Column
	@NotNull
	@Override
	public String getName() {
		return super.getName();
	}
	
	@Column
	@NotNull
	@Override
	public String getDescription() {
		return super.getDescription();
	}
	
	public ProjectEntity(String name, String description) {
		super(name, description);
	}
}

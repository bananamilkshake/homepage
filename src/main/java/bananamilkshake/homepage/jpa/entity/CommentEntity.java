/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.jpa.entity;

import bananamilkshake.homepage.model.Comment;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DiscriminatorOptions;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "comments")
@Access(AccessType.PROPERTY)
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class CommentEntity extends Comment {

	private List<AnswerCommentEntity> answers;
	
	private PrincipalEntity principal;
	
	@Id
	@Override
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comments_seq_gen")
	@SequenceGenerator(name = "comments_seq_gen", sequenceName = "comments_id_seq", allocationSize = 1)
	public long getId() {
		return super.getId();
	}

	@NotNull
	@Override
	@Column(name = "creator_name")
	public String getCreatorName() {
		return super.getCreatorName();
	}

	@Override
	@Column(length = 255)
	public String getEmail() {
		return super.getEmail();
	}

	@NotNull
	@Override
	@Column(length = 200)
	public String getText() {
		return super.getText();
	}

	@NotNull
	@Override
	@Column(name = "creation_date")
	public ZonedDateTime getCreationDate() {
		return super.getCreationDate();
	}

	@OneToMany(mappedBy = "answerTo")
	public List<AnswerCommentEntity> getAnswers() {
		return answers;
	}

	@ManyToOne
	public PrincipalEntity getPrincipal() {
		return principal;
	}

	/**
	 * @return Does author of this comment must be notified about answer
	 */
	@Transient
	public boolean isNeedToSendAnswer() {
		return getPrincipal() == null && getEmail() != null;
	}
	
	public CommentEntity(PrincipalEntity principal, String creatorName, String email, String text, ZonedDateTime date) {
		super(creatorName, email, text, date);
		this.setPrincipal(principal);
		this.setAnswers(new ArrayList<>());
	}
}

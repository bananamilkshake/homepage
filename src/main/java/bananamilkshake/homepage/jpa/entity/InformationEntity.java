package bananamilkshake.homepage.jpa.entity;

import bananamilkshake.homepage.model.Information;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "information")
@Access(AccessType.PROPERTY)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InformationEntity extends Information {

	private long id;

	@Id
	@Column
	public long getId() {
		return id;
	}

	@Override
	@Column(name = "full_info")
	public String getFullInfo() {
		return super.getFullInfo();
	}

	@Override
	@Column(name = "short_info")
	public String getShortInfo() {
		return super.getShortInfo();
	}

	@Override
	@Column(name = "real_name")
	public String getRealName() {
		return super.getRealName();
	}

	@Column
	@Override
	public String getName() {
		return super.getName();
	}

	@Column
	@Override
	public String getEmail() {
		return super.getEmail();
	}

	@Override
	@Column(name = "linkedin_link")
	public String getLinkedinLink() {
		return super.getLinkedinLink();
	}

	@Override
	@Column(name = "bitbucket_link")
	public String getBitbucketLink() {
		return super.getBitbucketLink();
	}

	@Override
	@Column(name = "github_link")
	public String getGithubLink() {
		return super.getGithubLink();
	}

	public InformationEntity(long id, Information source) {
		super(source);
		this.id = id;
	}
}

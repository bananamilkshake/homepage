/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.jpa.entity;

import bananamilkshake.homepage.model.Article;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "articles")
@Access(AccessType.PROPERTY)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ArticleEntity extends Article {

	protected List<RootCommentEntity> rootComments = new ArrayList<>();
	
	@Id
	@Column
	@Override
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "articles_seq_gen")
	@SequenceGenerator(name = "articles_seq_gen", sequenceName = "articles_id_seq", allocationSize = 1)
	public Long getId() {
		return super.getId();
	}
	
	@NotNull
	@Override
	public String getTitle() {
		return super.getTitle();
	}

	@NotNull
	@Override
	public String getText() {
		return super.getText();
	}
	
	@Override
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "article_tags", joinColumns = @JoinColumn(name = "article_id"))
	public List<String> getTags() {
		return super.getTags();
	}
	
	@Override
	@Column(name = "creation_date")
	public ZonedDateTime getCreationDate() {
		return super.getCreationDate();
	}
	
	@Column
	@Override
	public boolean isPublished() {
		return super.isPublished();
	}
	
	@OrderBy("creationDate DESC")
	@OneToMany(mappedBy = "article")
	public List<RootCommentEntity> getRootComments() {
		return rootComments;
	}

	public ArticleEntity(String title, String text, List<String> tags, ZonedDateTime creationDate, boolean published) {
		super(title, text, tags, creationDate, published);
	}
}

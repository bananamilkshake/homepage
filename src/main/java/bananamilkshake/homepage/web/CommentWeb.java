/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.web;

import bananamilkshake.homepage.model.Comment;

import java.time.ZonedDateTime;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CommentWeb extends Comment {

	private static final long serialVersionUID = 1L;

	private List<CommentWeb> answers;
	
	private PrincipalWeb principal;

	@Override
	public String getCreatorName() {
		if (principal != null) {
			return principal.getUsername();
		} else {
			return super.getCreatorName();
		}
	}
	
	public CommentWeb(long id, String creatorName, String email, String text, ZonedDateTime date, PrincipalWeb principal, List<CommentWeb> answers) {
		super(id, creatorName, email, text, date);
		this.setPrincipal(principal);
		this.setAnswers(answers);
	}
}

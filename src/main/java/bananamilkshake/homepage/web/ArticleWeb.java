/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.web;

import bananamilkshake.homepage.model.Article;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ArticleWeb extends Article {

	private static final long serialVersionUID = 1L;
	
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	static {
		OBJECT_MAPPER.registerModule(new JavaTimeModule());
	}

	private List<CommentWeb> rootComments;

	public ArticleWeb(Long id, String title, String text, List<String> tags, ZonedDateTime creationDate, boolean published) {
		super(id, title, text, tags, creationDate, published);
	}
	
	public ArticleWeb(Long id, String title, String text, List<String> tags, ZonedDateTime creationDate, boolean published, List<CommentWeb> comments) {
		super(id, title, text, tags, creationDate, published);
		this.setRootComments(comments);
	}

	public String asJson() throws JsonProcessingException {
		return OBJECT_MAPPER.writeValueAsString(this);
	}
}

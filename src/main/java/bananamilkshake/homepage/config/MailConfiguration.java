package bananamilkshake.homepage.config;

import bananamilkshake.homepage.mail.AnswerNotificationProducer;
import bananamilkshake.homepage.mail.AnswersNotificationManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Slf4j
@Configuration
public class MailConfiguration {

	@Autowired
	private Properties systemProperties;

	private static final String PROPERTY_HOST = "homepage.mail.host";
	private static final String PROPERTY_PORT = "homepage.mail.port";
	private static final String PROPERTY_USERNAME = "homepage.mail.username";
	private static final String PROPERTY_PASSWORD = "homepage.mail.password";
	private static final String PROPERTY_SMTP_CONNECTION_TIMEOUT = "homepage.mail.connectionTimeout";

	private String getHost() {
		return systemProperties.getProperty(PROPERTY_HOST);
	}

	private int getPort() {
		return Integer.getInteger(PROPERTY_PORT);
	}

	private String getUsername() {
		return systemProperties.getProperty(PROPERTY_USERNAME);
	}

	private String getPassword() {
		return systemProperties.getProperty(PROPERTY_PASSWORD);
	}

	private int getConnectionTimeout() {
		return Integer.getInteger(PROPERTY_SMTP_CONNECTION_TIMEOUT);
	}

	@Bean
	public JavaMailSender javaMailSender() {
		Properties prop = new Properties();
		prop.put("mail.smtp.ssl.enable","true");
		prop.put("mail.smtp.connectiontimeout", getConnectionTimeout());
		prop.put("mail.smtp.timeout", getConnectionTimeout());
		prop.put("mail.smtp.writetimeout", getConnectionTimeout());

		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		javaMailSenderImpl.setHost(getHost());
		javaMailSenderImpl.setPort(getPort());
		javaMailSenderImpl.setUsername(getUsername());
		javaMailSenderImpl.setPassword(getPassword());
		javaMailSenderImpl.setJavaMailProperties(prop);

		return javaMailSenderImpl;
	}

	@Bean
	public VelocityEngine velocityEngine() {
		VelocityEngine engine = new VelocityEngine();
		engine.addProperty("resource.loader", "class");
		engine.addProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		return engine;
	}

	@Bean
	public AnswerNotificationProducer answersNotificationProducer() {
		return new AnswerNotificationProducer();
	}
}

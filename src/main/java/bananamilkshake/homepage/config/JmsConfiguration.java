package bananamilkshake.homepage.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.SimpleJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jndi.JndiTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.NamingException;

@EnableJms
@Configuration
public class JmsConfiguration {

	private static final String CONNECTION_FACTORY_JNDI_NAME = "java:/jms/HomepageConnectionFactory";

	private static final String JMS_MAIL_QUEUE_JNDI_NAME = "java:/jms/HomepageMailQueue";

	private static final String JMS_CONNECTION_CACHE_SIZE = "homepage.jms.connectionsCacheSize";

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerContainerFactory() throws NamingException {
		SimpleJmsListenerContainerFactory factory = new SimpleJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory());
		factory.setSessionTransacted(true);
		return factory;
	}

	@Bean
	public JmsTemplate jmsTemplate() throws NamingException {
		JmsTemplate jmsTemplate = new JmsTemplate();
		jmsTemplate.setConnectionFactory(connectionFactory());
		jmsTemplate.setDeliveryPersistent(true);
		jmsTemplate.setSessionTransacted(true);
		jmsTemplate.setSessionAcknowledgeMode(Session.SESSION_TRANSACTED);
		return jmsTemplate;
	}

	@Bean
	public ConnectionFactory connectionFactory() throws NamingException {
		ConnectionFactory targetConnectionFactory = new JndiTemplate().lookup(CONNECTION_FACTORY_JNDI_NAME, ConnectionFactory.class);
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(targetConnectionFactory);
		connectionFactory.setReconnectOnException(true);
		connectionFactory.setSessionCacheSize(Integer.getInteger(JMS_CONNECTION_CACHE_SIZE));
		return connectionFactory;
	}

	@Bean
	public Destination mailQueue() throws NamingException {
		return new JndiTemplate().lookup(JMS_MAIL_QUEUE_JNDI_NAME, Queue.class);
	}
}

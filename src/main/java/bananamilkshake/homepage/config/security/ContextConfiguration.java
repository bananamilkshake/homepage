package bananamilkshake.homepage.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class ContextConfiguration {

	@Bean
	public Properties systemProperties() {
		return System.getProperties();
	}
}

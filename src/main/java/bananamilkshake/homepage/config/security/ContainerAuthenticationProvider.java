/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.config.security;

import bananamilkshake.homepage.service.security.UserDetailsServiceImpl;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Slf4j
@Component
public class ContainerAuthenticationProvider implements AuthenticationProvider {
	
	@Setter
	private UserDetailsService userDetailsService;
	
	@Setter
	private PasswordEncoder encoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		UsernamePasswordAuthenticationToken authToken = (UsernamePasswordAuthenticationToken) authentication;
		HttpServletRequest request = getHttpServletRequest();

		final String principal = authToken.getPrincipal().toString();
		final String password = authToken.getCredentials().toString();
		
		try {
			request.login(principal, password);
		} catch (ServletException exception) {
			throw new BadCredentialsException("Login with application server failed", exception);
		}

		try {
			Set<String> authorities = Roles.ALL
					.stream()
					.filter(request::isUserInRole)
					.collect(Collectors.toSet());

			log.debug("Authenticated user from web server: name {}, authorities {}", principal, Arrays.toString(authorities.toArray()));

			UserDetails user = ((UserDetailsServiceImpl) userDetailsService).loadWebContainerUser(principal, encoder.encode(password), authorities);

			return new UsernamePasswordAuthenticationToken(user, password, user.getAuthorities());
		} catch (Exception exception) {
			try {
				request.logout();
			} catch (ServletException logoutException) {
				log.warn("HttpServletRequest.logout() failed", logoutException);
			}

			throw new BadCredentialsException("Failed to load principal from userDetailsService", exception);
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
	
	/**
	 * @return HttpServletRequest for current thread.
	 */
	private HttpServletRequest getHttpServletRequest() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
		return request;
	}
}

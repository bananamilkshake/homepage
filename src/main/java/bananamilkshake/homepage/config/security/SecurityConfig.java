/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.config.security;

import bananamilkshake.homepage.service.security.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.servlet.support.csrf.CsrfRequestDataValueProcessor;

@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	public static final String URL_LOGOUT = "/logout";
	
	private final UserDetailsServiceImpl userDetailsServiceImpl = new UserDetailsServiceImpl();
	
	private final BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		log.info("Create WebSecurityConfigurerAdapter");
		http
			.authenticationProvider(authenticationProvider())
			.authorizeRequests()
				.antMatchers("/admin/**").hasAnyAuthority(Roles.ADMIN)
				.antMatchers("/**").permitAll()
				.and()
			.exceptionHandling()
				.authenticationEntryPoint(authenticationEntryPoint())
				.and()
			.formLogin()
				.successHandler(restAuthenticationSuccessHandler())
				.failureHandler(restAuthenticationFailureHandler())
				.permitAll()
				.and()
			.logout()
				.logoutUrl(URL_LOGOUT)
				.logoutSuccessUrl("/")
				.invalidateHttpSession(true)
				.addLogoutHandler(logoutHandler())
				.permitAll();
	}

	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		log.info("Configure global security");
		
		auth
			.userDetailsService(userDetailsService())
				.passwordEncoder(passwordEncoder());
	}

	/* This is needed to make Thymeleaf place CSRF tokens
	 * to any request.
	 */
	@Bean
	public CsrfRequestDataValueProcessor requestDataValueProcessor() {
		return new CsrfRequestDataValueProcessor();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return bcryptPasswordEncoder;
	}
	
	/* UserDateilsService is responsible for retrieving
	principal from application repository by his username
	and password.
	*/
	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		return userDetailsServiceImpl;
	}
	
	@Bean
	public FilterChainProxy filterChainProxy() {
		return new FilterChainProxy();
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		ContainerAuthenticationProvider containerAuthenticationProvider = new ContainerAuthenticationProvider();
		containerAuthenticationProvider.setUserDetailsService(userDetailsServiceImpl);
		containerAuthenticationProvider.setEncoder(passwordEncoder());
		return containerAuthenticationProvider;
	}

	private AuthenticationEntryPoint authenticationEntryPoint() {
		return new Http403ForbiddenEntryPoint();
	}
	
	private AuthenticationSuccessHandler restAuthenticationSuccessHandler() {
		return new RestAuthenticationSuccessHandler();
	}
	
	private AuthenticationFailureHandler restAuthenticationFailureHandler() {
		return new RestAuthenticationFailureHandler();
	}

	private LogoutHandler logoutHandler() {
		return new ApplicationServerLogoutHandler();
	}
}

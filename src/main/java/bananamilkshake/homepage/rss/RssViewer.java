package bananamilkshake.homepage.rss;

import bananamilkshake.homepage.web.ArticleWeb;
import com.rometools.rome.feed.atom.Content;
import com.rometools.rome.feed.atom.Entry;
import com.rometools.rome.feed.atom.Link;
import org.springframework.web.servlet.view.feed.AbstractAtomFeedView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.text.MessageFormat.format;

public class RssViewer extends AbstractAtomFeedView {

	@Override
	protected List<Entry> buildFeedEntries(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Object modelObject = model.get("feedContent");
		List<ArticleWeb> listContent = (List<ArticleWeb>) modelObject;

		List<Entry> entries = new ArrayList<>(listContent.size());

		for (ArticleWeb article : listContent) {
			Entry entry = new Entry();

			entry.setId(article.getId().toString());
			entry.setTitle(article.getTitle());
			entry.setCreated(Date.from(article.getCreationDate().toInstant()));
			entry.setSummary(createContent(article));
			entry.setOtherLinks(createLinks(article, request));

			entries.add(entry);
		}

		return entries;
	}

	private Content createContent(ArticleWeb article) {
		Content content = new Content();
		content.setValue(article.getText());
		return content;
	}

	private List<Link> createLinks(ArticleWeb article, HttpServletRequest request) {

		Link link = new Link();
		String href = format("{0}/articles/{1}/", request.getContextPath(), article.getId());
		link.setHref(href);

		List<Link> links = new ArrayList<>();
		links.add(link);
		return links;
	}
}

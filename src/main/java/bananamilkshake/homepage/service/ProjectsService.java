/*
 * Copyright (C) 2016 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package bananamilkshake.homepage.service;

import bananamilkshake.homepage.config.security.Roles;
import bananamilkshake.homepage.jpa.entity.ProjectEntity;
import bananamilkshake.homepage.jpa.repository.ProjectsRepository;
import bananamilkshake.homepage.web.ProjectWeb;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import javax.transaction.Transactional;

@Service
@Transactional
public class ProjectsService {

	@Autowired
	private ProjectsRepository projectsRepository;

	private static ProjectWeb convertProjectEntity(ProjectEntity entity) {
		return new ProjectWeb(entity.getId(), entity.getName(), entity.getDescription());
	}
	
	public ProjectWeb getProject(String name) {
		ProjectEntity projectEntity = projectsRepository.findByName(name);
		return convertProjectEntity(projectEntity);
	}
	
	public Iterable<ProjectWeb> getProjects() {
		List<ProjectEntity> projectsEntity = projectsRepository.findAll();
		return projectsEntity
			.stream()
			.map(ProjectsService::convertProjectEntity)
			.collect(Collectors.toList());
	}

	@RolesAllowed(Roles.ADMIN)
	public ProjectWeb addProject(String name, String description) {
		ProjectEntity projectEntity = new ProjectEntity(name, description);
		projectEntity = projectsRepository.save(projectEntity);
		return convertProjectEntity(projectEntity);
	}

	@RolesAllowed(Roles.ADMIN)
	public ProjectWeb editProject(long id, String name, String description) {
		ProjectEntity projectEntity = projectsRepository.findOne(id);
		projectEntity.setName(name);
		projectEntity.setDescription(description);
		projectEntity = projectsRepository.save(projectEntity);
		return convertProjectEntity(projectEntity);
	}
}

package bananamilkshake.homepage.service;

import bananamilkshake.homepage.config.security.Roles;
import bananamilkshake.homepage.exceptions.NonExistentInformationException;
import bananamilkshake.homepage.jpa.entity.InformationEntity;
import bananamilkshake.homepage.jpa.repository.InformationRepository;
import bananamilkshake.homepage.model.Information;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class InformationService {

	private static final String INFORMATION_CACHE_EXPIRATION_MINUTES_PROPERTY = "homepage.information.cacheExpiration";
	private static final String INFORMATION_ID_PROPERTY = "homepage.information.id";

	@Autowired
	private Properties systemProperties;

	@Autowired
	private InformationRepository informationRepository;

	private LoadingCache<Long, Optional<Information>> cache;

	@PostConstruct
	public void postConstruct() {
		int expirationMinutes = Integer.parseInt(systemProperties.getProperty(INFORMATION_CACHE_EXPIRATION_MINUTES_PROPERTY));

		cache = CacheBuilder
				.newBuilder()
				.expireAfterAccess(expirationMinutes, TimeUnit.MINUTES)
				.build(new CacheLoader<Long, Optional<Information>>() {
					@Override
					public Optional<Information> load(Long key) throws Exception {
						log.debug("Loading expired information from repository");
						return Optional.of(retrieveInformation(key));
					}
				});
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Information retrieveInfo() throws ExecutionException, NonExistentInformationException {
		long informationId = getInformationId();
		return cache
				.get(informationId)
				.orElseThrow(NonExistentInformationException::new);
	}

	@Secured(Roles.ADMIN)
	public Information getInfo() {
		long id = getInformationId();
		return retrieveInformation(id);
	}

	@Secured(Roles.ADMIN)
	public boolean update(Information information) {
		long id = getInformationId();
		try {
			InformationEntity entity = new InformationEntity(id, information);
			informationRepository.save(entity);
			cache.refresh(id);
			return true;
		} catch (Exception exception) {
			log.error(MessageFormat.format("Exception on updating information {} with values {}", id, information), exception);
			return false;
		}
	}

	private long getInformationId() {
		return Long.parseLong(systemProperties.getProperty(INFORMATION_ID_PROPERTY));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private Information retrieveInformation(long id) {
		InformationEntity entity = informationRepository.findOne(id);
		if (entity == null) {
			return new Information();
		}
		return new Information(entity);
	}
}

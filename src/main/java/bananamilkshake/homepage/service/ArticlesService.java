/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.service;

import bananamilkshake.homepage.config.security.Roles;
import bananamilkshake.homepage.jpa.entity.AnswerCommentEntity;
import bananamilkshake.homepage.jpa.entity.ArticleEntity;
import bananamilkshake.homepage.jpa.entity.CommentEntity;
import bananamilkshake.homepage.jpa.entity.PrincipalEntity;
import bananamilkshake.homepage.jpa.entity.RootCommentEntity;
import bananamilkshake.homepage.exceptions.NonExistentCommentException;
import bananamilkshake.homepage.exceptions.NonExistentArticleException;

import java.time.ZonedDateTime;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.transaction.Transactional;

import bananamilkshake.homepage.jpa.repository.AnswerCommentsRepository;
import bananamilkshake.homepage.jpa.repository.CommentsRepository;
import bananamilkshake.homepage.jpa.repository.ArticlesRepository;
import bananamilkshake.homepage.jpa.repository.RootCommentsRepository;
import bananamilkshake.homepage.mail.AnswerNotification;
import bananamilkshake.homepage.mail.AnswerNotificationProducer;
import bananamilkshake.homepage.web.ArticleWeb;
import bananamilkshake.homepage.web.CommentWeb;
import bananamilkshake.homepage.web.PrincipalWeb;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Transactional
public class ArticlesService {

	@Autowired
	private ArticlesRepository articlesRepository;

	@Autowired
	private CommentsRepository commentsRepository;

	@Autowired
	private RootCommentsRepository rootCommentsRepository;

	@Autowired
	private AnswerCommentsRepository answerCommentsRepository;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private AnswerNotificationProducer answersNotificationProducer;
	
	private static PrincipalWeb convertPrincipalEntity(PrincipalEntity entity) {
		return entity != null ? new PrincipalWeb(entity.getUsername(), entity.getAuthorities()) : null;
	}

	private static CommentWeb convertCommentEntity(CommentEntity entity) {
		List<CommentWeb> answersWeb = entity.getAnswers()
			.stream()
			.map(ArticlesService::convertCommentEntity)
			.collect(Collectors.toList());
		return new CommentWeb(
			entity.getId(),
			entity.getCreatorName(),
			entity.getEmail(),
			entity.getText(),
			entity.getCreationDate(),
			convertPrincipalEntity(entity.getPrincipal()),
			answersWeb);
	}
	
	private static List<CommentWeb> convertCommentsEntity(List<? extends CommentEntity> commentsEntity) {
		return commentsEntity
			.stream()
			.map(ArticlesService::convertCommentEntity)
			.collect(Collectors.toList());
	}
	
	private static ArticleWeb convertArticle(ArticleEntity entity) {
		ArticleWeb web = convertArticleSimple(entity);
		List<CommentWeb> commentsWeb = convertCommentsEntity( entity.getRootComments());
		web.setRootComments(commentsWeb);
		return web;
	}
	
	/**
	 * Convert ArticleEntity to web representation. Target object will not contain
	 * information about comments.
	 * @return web representation of articles with loaded comments (not null)
	 */
	private static ArticleWeb convertArticleSimple(ArticleEntity entity) {
		return new ArticleWeb(entity.getId(),
			entity.getTitle(),
			entity.getText(),
			entity.getTags(),
			entity.getCreationDate(),
			entity.isPublished());
	}

	private static List<ArticleWeb> convertArticles(List<ArticleEntity> articlesEntity) {
		return articlesEntity
				.stream()
				.map(ArticlesService::convertArticleSimple)
				.collect(Collectors.toList());
	}

	@Transactional(Transactional.TxType.MANDATORY)
	private ArticleEntity getArticleEntity(long id) {
		ArticleEntity articleEntity = articlesRepository.findOneByIdAndPublishedTrue(id);
		if (articleEntity == null) {
			log.warn("No article with id {} found", id);
			throw new NonExistentArticleException(id);
		}
		
		return articleEntity;
	}
	
	@Transactional(Transactional.TxType.MANDATORY)
	private CommentEntity getCommentEntity(long id) {
		CommentEntity commentEntity = commentsRepository.findOne(id);
		if (commentEntity == null) {
			log.warn("No comment with id {} found", id);
			throw new NonExistentCommentException(id);
		}
		return commentEntity;
	}

	private void addComment(long articleId, String creatorName, PrincipalEntity creator, String email, String comment, Long answerTo) {
		ArticleEntity article = articlesRepository.findOneByIdAndPublishedTrue(articleId);
		ZonedDateTime currentDate = ZonedDateTime.now();

		if (answerTo == null) {
			RootCommentEntity newComment = new RootCommentEntity(article, creator, creatorName, email, comment, currentDate);
			rootCommentsRepository.save(newComment);
		} else {
			CommentEntity answerToComment = getCommentEntity(answerTo);
			AnswerCommentEntity newComment = new AnswerCommentEntity(creator, creatorName, email, comment, answerToComment, currentDate);
			newComment = answerCommentsRepository.save(newComment);

			if (answerToComment.isNeedToSendAnswer()) {
				AnswerNotification notification = new AnswerNotification(articleId, newComment.getId(), answerToComment.getCreatorName(), answerToComment.getEmail(), creatorName, servletContext.getContextPath());
				answersNotificationProducer.send(notification);
			}
		}
	}

	/**
	 * Retrieves record with id {@id} from repository and returns it. If no 
	 * record with such id exists throws {@NonExistentRecord} exception.
	 *
	 * @param id record id
	 * @return
	 */
	public ArticleWeb getArticle(long id) {
		return convertArticle(getArticleEntity(id));
	}

	public Page<ArticleWeb> getPage(Integer block, int blockSize, Sort sort) {
		Pageable pageRequest = new PageRequest(block, blockSize, sort);
		List<ArticleEntity> articlesEntity = articlesRepository.findByPublishedTrue(pageRequest);
		List<ArticleWeb> articlesWeb = convertArticles(articlesEntity);
		return new PageImpl<>(articlesWeb, pageRequest, articlesWeb.size());
	}

	public List<ArticleWeb> getAllPublished() {
		List<ArticleEntity> articlesEntity = articlesRepository.findByPublishedTrueOrderByCreationDateDesc();
		return convertArticles(articlesEntity);
	}

	public Page<ArticleWeb> getPageWithTag(String tag, Integer block, Integer blockSize, Sort sort) {
		Pageable pageRequest = new PageRequest(block, blockSize, sort);
		List<ArticleEntity> articlesEntity = articlesRepository.findByTagsAndPublishedTrue(tag, pageRequest);
		List <ArticleWeb> articlesWeb = articlesEntity
			.stream()
			.map(ArticlesService::convertArticleSimple)
			.collect(Collectors.toList());
		return new PageImpl<>(articlesWeb, pageRequest, articlesWeb.size());
	}

	/**
	 * @param articleId new comment must be added to article with this id
	 * @param creatorName name of user that created comment
	 * @param email
	 * @param comment comment text
	 * @param answerTo id of comment on which new comment must answer (can be null if
	 * comment is added directly to article)
	 */
	public void addComment(long articleId, String creatorName, String email, String comment, Long answerTo) {
		addComment(articleId, creatorName, null, email, comment, answerTo);
	}

	/**
	 * @param articleId new comment must be added to article with this id
	 * @param creator logged user that created this comment
	 * @param email 
	 * @param comment comment text
	 * @param answerTo id of comment on which new comment must answer (can be null if
	 * comment is added directly to article)
	 */
	public void addComment(long articleId, PrincipalEntity creator, String email, String comment, Long answerTo) {
		addComment(articleId, creator.getUsername(), creator, email, comment, answerTo);
	}

	public List<CommentWeb> getComments(long articleId) {
		ArticleEntity article = this.getArticleEntity(articleId);
		List<RootCommentEntity> commentsEntity = rootCommentsRepository.findAllByArticleOrderByCreationDateDesc(article);
		return convertCommentsEntity(commentsEntity);
	}

	@RolesAllowed({Roles.ADMIN})
	public Page<ArticleWeb> getAllPage(int block, Integer blockSize, Sort sort) {
		Pageable pageRequest = new PageRequest(block, blockSize, sort);
		Page<ArticleEntity> articlesEntity = articlesRepository.findAll(pageRequest);
		List <ArticleWeb> articlesWeb = convertArticles(articlesEntity.getContent());
		return new PageImpl<>(articlesWeb, pageRequest, articlesEntity.getTotalElements());
	}

	@RolesAllowed({Roles.ADMIN})
	public ArticleWeb add(String title, String text, List<String> tags, boolean published) {
		ArticleEntity newArticle = articlesRepository.save(new ArticleEntity(title, text, tags, ZonedDateTime.now(), published));
		log.debug("New article added: {}", newArticle);
		return convertArticle(newArticle);
	}

	@RolesAllowed({Roles.ADMIN})
	public ArticleWeb edit(long id, String title, String text, List<String> tags, boolean published) {
		ArticleEntity articleEntity = articlesRepository.findOne(id);
		articleEntity.setTitle(title);
		articleEntity.setText(text);
		articleEntity.setTags(tags);
		articleEntity.setPublished(published);
		articleEntity = articlesRepository.save(articleEntity);
		log.debug("Article updated: {}", articleEntity);
		return convertArticle(articleEntity);
	}
}

/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.service.security;

import bananamilkshake.homepage.jpa.entity.PrincipalEntity;
import bananamilkshake.homepage.jpa.repository.PrincipalsRepository;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;

import bananamilkshake.homepage.model.Principal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Slf4j
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private PrincipalsRepository principalsRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		PrincipalEntity principal = loadPrincipalEntity(username)
			.orElseThrow(() -> new UsernameNotFoundException(String.format("No user with name \"%s\"", username)));

		log.debug("Load user directly from datasource: username {}, roles {}", principal.getUsername(), Arrays.asList(principal.getAuthorities().toArray()));
		
		return createUserDetailsFromPrincipal(principal);
	}
	
	public UserDetails loadWebContainerUser(String username, String password, Set<String> authorities) {
		
		log.debug("Load web container user data from datasource: username {}, roles {}", username, Arrays.asList(authorities.toArray()));

		PrincipalEntity principal = loadPrincipalEntity(new PrincipalEntity(username, authorities, password))
			.orElseGet(() -> savePrincipal(username, password, authorities));
		
		return createUserDetailsFromPrincipal(principal);
	}
	
	@Transactional(Transactional.TxType.MANDATORY)
	private Optional<PrincipalEntity> loadPrincipalEntity(String username) {
		PrincipalEntity principal = principalsRepository.findOne(username);
		return Optional.ofNullable(principal);
	}

	/**
	 * This method is for loading user that was created from application server user. If saves
	 * information about user if it is not equal to old info.
	 *
	 * @param principal Principal entity that is neded to be loaded from databse.
	 * @return principal entity loaded from database or null if there no info.
	 */
	@Transactional(Transactional.TxType.MANDATORY)
	private Optional<PrincipalEntity> loadPrincipalEntity(PrincipalEntity principal) {
		PrincipalEntity loadedPrincipal = principalsRepository.findOne(principal.getUsername());
		if (!principal.equals(loadedPrincipal)) {
			return Optional.empty();
		}

		return Optional.ofNullable(loadedPrincipal);
	}


	private PrincipalEntity savePrincipal(String username, String password, Set<String> authorities) {
		log.debug("Creating new principal from web server user: username {}, roles {}", username, Arrays.asList(authorities.toArray()));
		PrincipalEntity newEntity = new PrincipalEntity(username, authorities, password);
		return principalsRepository.save(newEntity);
	}

	static private List<SimpleGrantedAuthority> extractAuthorities(PrincipalEntity principal) {
		return principal.getAuthorities()
			.stream()
			.map(SimpleGrantedAuthority::new)
			.collect(Collectors.toList());
	}

	private UserDetails createUserDetailsFromPrincipal(PrincipalEntity principal) {
		return new User(principal.getUsername(),
			principal.getPassword(),
			true,
			true,
			true,
			true,
			extractAuthorities(principal));
	}
}

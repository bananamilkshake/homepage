/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.controller.validation.CommentValidation;
import bananamilkshake.homepage.exceptions.CommentFieldsValidationException;
import bananamilkshake.homepage.jpa.entity.PrincipalEntity;
import bananamilkshake.homepage.exceptions.NonExistentUserException;
import bananamilkshake.homepage.jpa.repository.PrincipalsRepository;
import bananamilkshake.homepage.service.ArticlesService;
import bananamilkshake.homepage.web.ArticleWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.MessageFormat;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/articles")
public class ArticlesController extends InformationController {

	private static final String FRAGMENT_COMMENTS = "fragments/article :: comments";

	// html file names for models
	public static final String VIEW_ARTICLES = "articles";
	public static final String VIEW_ARTICLE = "article";
	public static final String VIEW_BY_TAG = "search";

	public static final String PARAMETER_ARTICLE = "article";
	public static final String PARAMETER_ARTICLES = "articles";
	public static final String PARAMETER_CURRENT_PAGE = "currentPage";
	public static final String PARAMETER_TAG = "tag";

	private static final Sort ARTICLES_SORT = new Sort(new Order(Direction.DESC, "creationDate"));

	private static final Integer BLOCK_SIZE = 5;

	@Autowired
	private ArticlesService articlesService;

	@Autowired
	private PrincipalsRepository principalsRepository;
	
	@Autowired
	private CommentValidation commentValidation;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView onGet(@RequestParam(defaultValue = "0") Integer block) throws Exception {
		Page<ArticleWeb> articles = articlesService.getPage(block, BLOCK_SIZE, ARTICLES_SORT);
		ModelAndView modelAndView = new ModelAndView(VIEW_ARTICLES);
		modelAndView.addObject(PARAMETER_ARTICLES, articles);
		modelAndView.addObject(PARAMETER_CURRENT_PAGE, block + 1);
		return modelAndView;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView onRecordGet(@PathVariable("id") Long id) throws IllegalArgumentException {
		ModelAndView modelAndView = new ModelAndView(VIEW_ARTICLE);
		modelAndView.addObject(PARAMETER_ARTICLE, articlesService.getArticle(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/{id}/comment", method = RequestMethod.POST)
	public String addComment(Model model,
		@PathVariable("id") long recordId,
		@RequestParam String name,
		@RequestParam(required = false) String email,
		@RequestParam String comment,
		@RequestParam(required = false) Long answerTo,
		@AuthenticationPrincipal User activeUser) throws NonExistentUserException, CommentFieldsValidationException {
		
		commentValidation.validateCommentsFields(name, comment);
		
		log.debug("New comment from {} for article {}, active user {}", name, recordId, activeUser);
		
		if (activeUser != null) {
			PrincipalEntity principal = principalsRepository.findOne(activeUser.getUsername());
			if (principal == null) {
				throw new NonExistentUserException(MessageFormat.format("No principal with username {0}", activeUser.getUsername()));
			}
			articlesService.addComment(recordId, principal, email, comment, answerTo);
		} else {
			articlesService.addComment(recordId, name, email, comment, answerTo);
		}

		model.addAttribute("comments", articlesService.getComments(recordId));
		return FRAGMENT_COMMENTS;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView onGetRecordsByTag(@RequestParam String tag, @RequestParam(defaultValue = "0") Integer block) {
		Page<ArticleWeb> articles = articlesService.getPageWithTag(tag, block, BLOCK_SIZE, ARTICLES_SORT);
		ModelAndView modelAndView = new ModelAndView(VIEW_BY_TAG);
		modelAndView.addObject(PARAMETER_TAG, tag);
		modelAndView.addObject(block);
		modelAndView.addObject(PARAMETER_ARTICLES, articles);
		return modelAndView;
	}
}

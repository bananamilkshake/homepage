package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.exceptions.NonExistentInformationException;
import bananamilkshake.homepage.model.Information;
import bananamilkshake.homepage.service.InformationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.concurrent.ExecutionException;

@Slf4j
public class InformationController {

	@Autowired
	private InformationService informationService;

	@ModelAttribute("information")
	public Information getInformation() {
		try {
			return informationService.retrieveInfo();
		} catch (ExecutionException | NonExistentInformationException e) {
			log.error("Exception on information retrieval", e);
			return null;
		}
	}
}

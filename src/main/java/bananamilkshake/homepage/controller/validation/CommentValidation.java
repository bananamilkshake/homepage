/*
 * Copyright (C) 2016 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller.validation;

import bananamilkshake.homepage.exceptions.CommentFieldsValidationException;
import java.util.HashMap;
import java.util.Map;

public class CommentValidation {

	/**
	 * Checks that submitted name and comment text are valid to use.
	 * 
	 * @param name name field for comment
	 * @param comment text of comment
	 * @throws CommentFieldsValidationException when name or comment are empty
	 */
	public void validateCommentsFields(String name, String comment) throws CommentFieldsValidationException {
		Map<String, String> filedsErrors = new HashMap<>();
		if (name.isEmpty()) {
			filedsErrors.put("name", "Cannot be empty");
		}
		if (comment.isEmpty()) {
			filedsErrors.put("comment", "Cannot be empty");
		}
		if (!filedsErrors.isEmpty()) {
			throw new CommentFieldsValidationException(filedsErrors);
		}
	}

}

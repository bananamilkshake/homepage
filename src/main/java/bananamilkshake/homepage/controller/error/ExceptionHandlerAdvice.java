/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller.error;

import bananamilkshake.homepage.exceptions.CommentFieldsValidationException;

import java.nio.file.AccessDeniedException;
import java.util.Map;
import javax.security.auth.message.AuthException;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerAdvice {

	@ExceptionHandler({AuthException.class, AccessDeniedException.class, AuthenticationException.class})
	public String onAuthException(HttpServletRequest request, Exception exception) {
		log.warn("Authentication error", exception);
		return "forward:/errors/401";
	}

	@ExceptionHandler({NoHandlerFoundException.class})
	public String onNotFound(HttpServletRequest request, Exception exception) {
		log.warn("Not found handling", exception);
		return "forward:/errors/404";
	}

	@ResponseBody
	@ExceptionHandler(CommentFieldsValidationException.class)
	public ResponseEntity<Map<String, String>> onCommentValidationFailed(CommentFieldsValidationException exception) {
		return new ResponseEntity<>(exception.getFiledsErrors(), HttpStatus.NOT_ACCEPTABLE);
	}
}

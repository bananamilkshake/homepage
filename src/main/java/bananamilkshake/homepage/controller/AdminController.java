/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.config.security.Roles;
import bananamilkshake.homepage.model.Information;
import bananamilkshake.homepage.service.InformationService;
import bananamilkshake.homepage.service.ProjectsService;
import bananamilkshake.homepage.service.ArticlesService;
import bananamilkshake.homepage.web.ArticleWeb;
import bananamilkshake.homepage.web.ProjectWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;

@Slf4j
@Controller
@Secured(Roles.ADMIN)
@RequestMapping("/admin")
public class AdminController {

	private static final String VIEW_INFORMATION = "admin/information";
	private static final String VIEW_ARTICLES = "admin/articles";
	private static final String VIEW_PROJECTS = "admin/projects";

	private static final String FRAGMENT_ARTICLE = "admin/fragments/articles :: article";
	private static final String FRAGMENT_PROJECT = "admin/fragments/projects :: project";

	private static final String PARAMETER_INFORMATION = "info";
	private static final String PARAMETER_ARTICLES = "articles";
	private static final String PARAMETER_ARTICLE = "article";
	private static final String PARAMETER_BLOCK = "pageId";
	private static final String PARAMETER_PROJECTS = "projects";
	private static final String PARAMETER_PROJECT = "project";

	private static final Sort RECORDS_SORT = new Sort(new Sort.Order(Sort.Direction.DESC, "creationDate"));

	private static final Integer BLOCK_SIZE = 5;

	@Autowired
	private ArticlesService articlesService;

	@Autowired
	private ProjectsService projectsService;

	@Autowired
	private InformationService informationService;

	@RequestMapping(value = "/articles", method = RequestMethod.GET, produces = {"text/json", "application/*"})
	public ModelAndView getArticles(@RequestParam(defaultValue = "0") int block) {
		ModelAndView modelAndView = new ModelAndView(VIEW_ARTICLES);
		modelAndView.addObject(PARAMETER_BLOCK, block + 1);
		modelAndView.addObject(PARAMETER_ARTICLES, articlesService.getAllPage(block, BLOCK_SIZE, RECORDS_SORT));
		return modelAndView;
	}

	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public ModelAndView getProjects() {
		ModelAndView modelAndView = new ModelAndView(VIEW_PROJECTS);
		modelAndView.addObject(PARAMETER_PROJECTS, projectsService.getProjects());
		return modelAndView;
	}

	@RequestMapping(value = "/articles/add", method = RequestMethod.POST)
	public ResponseEntity<?> createArticle(@RequestParam String title,
						@RequestParam String text,
						@RequestParam List<String> tags,
						@RequestParam boolean published) {
		try {
			articlesService.add(title, text, tags, published);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception exception) {
			return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/articles/edit", method = RequestMethod.POST)
	public String editArticle(Model model,
					@RequestParam long id,
					@RequestParam String title,
					@RequestParam String text,
					@RequestParam List<String> tags,
					@RequestParam boolean published) {
		ArticleWeb article = articlesService.edit(id, title, text, tags, published);
		model.addAttribute(PARAMETER_ARTICLE, article);
		return FRAGMENT_ARTICLE;
	}

	@RequestMapping(value = "/projects/add", method = RequestMethod.POST)
	public ResponseEntity<?> createProject(@RequestParam String name, @RequestParam String description) {
		try {
			projectsService.addProject(name, description);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception exception) {
			return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/projects/edit", method = RequestMethod.POST)
	public String editProject(Model model,
				  @RequestParam long id,
				  @RequestParam String name,
				  @RequestParam String description) {
		ProjectWeb project = projectsService.editProject(id, name, description);
		model.addAttribute(PARAMETER_PROJECT, project);
		return FRAGMENT_PROJECT;
	}

	@RequestMapping(value = "/information", method = RequestMethod.GET)
	public ModelAndView getInformation() {
		ModelAndView modelAndView = new ModelAndView(VIEW_INFORMATION);
		modelAndView.addObject(PARAMETER_INFORMATION, informationService.getInfo());
		return modelAndView;
	}

	@RequestMapping(value = "/information/update", method = RequestMethod.POST)
	public ResponseEntity<?> updateInformation(Information information) {
		if (informationService.update(information)) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

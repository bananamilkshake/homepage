package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.service.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RssController {

	@Autowired
	private ArticlesService articlesService;

	@RequestMapping(value = "/rssfeed", method = RequestMethod.GET)
	public ModelAndView getFeedItems() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("rssViewer");
		modelAndView.addObject("feedContent", articlesService.getAllPublished());
		return modelAndView;
	}
}

/*
 * Copyright (C) 2016 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.service.ProjectsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/projects")
public class ProjectsController extends InformationController {

	public static final String VIEW_PROJECTS = "projects";

	public static final String PARAMETER_PROJECTS = "projects";

	@Autowired
	private ProjectsService projectsService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView onGet() throws Exception {
		ModelAndView modelAndView = new ModelAndView(VIEW_PROJECTS);
		modelAndView.addObject(PARAMETER_PROJECTS, projectsService.getProjects());
		return modelAndView;
	}
}

package bananamilkshake.homepage.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class Information implements Serializable {

	private String fullInfo;

	private String shortInfo;
	private String realName;
	private String name;
	private String email;

	private String linkedinLink;
	private String bitbucketLink;
	private String githubLink;

	public Information(Information other) {
		this.setFullInfo(other.getFullInfo());
		this.setShortInfo(other.getShortInfo());
		this.setRealName(other.getRealName());
		this.setName(other.getName());
		this.setEmail(other.getEmail());
		this.setLinkedinLink(other.getLinkedinLink());
		this.setBitbucketLink(other.getBitbucketLink());
		this.setGithubLink(other.getGithubLink());
	}
}

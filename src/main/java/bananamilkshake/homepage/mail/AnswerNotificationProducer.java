package bananamilkshake.homepage.mail;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AnswerNotificationProducer {

	@Autowired
	private JmsTemplate jmsTemplate;

	public void send(AnswerNotification notification) {
		log.debug("Send new notification: from {} to {} (email {})", notification.getAuthorName(), notification.getTargetName(), notification.getTargetEmail());
		MessageCreator messageCreator = session -> session.createObjectMessage(notification);
		jmsTemplate.send("HomepageMailQueue", messageCreator);
	}
}

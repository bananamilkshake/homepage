package bananamilkshake.homepage.mail;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class AnswerNotification implements Serializable {

	private static final long serialVersionUID = 1L;

	private long articleId;
	private long commentId;

	private String targetName;
	private String targetEmail;

	private String authorName;

	private String contextPath;
}

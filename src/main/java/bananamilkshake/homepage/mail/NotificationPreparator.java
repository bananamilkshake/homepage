package bananamilkshake.homepage.mail;

import lombok.AllArgsConstructor;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.Map;

@AllArgsConstructor
public class NotificationPreparator implements MimeMessagePreparator {

	private VelocityEngine velocityEngine;

	Map<String, Object> model;

	private String subject;

	private String fromEmail;
	private String targetEmail;

	private String templatePath;

	@Override
	public void prepare(MimeMessage mimeMessage) throws Exception {
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		message.setTo(targetEmail);
		message.setFrom(fromEmail);
		message.setSubject(subject);
		message.setText(convert(model), true);
	}

	private String convert(Map model) {
		StringWriter result = new StringWriter();
		VelocityContext velocityContext = new VelocityContext(model);
		velocityEngine.mergeTemplate(templatePath, "UTF-8", velocityContext, result);
		return result.toString();
	}
}

package bananamilkshake.homepage.mail;

import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Slf4j
@Component
public class AnswersNotificationManager {

	private static final String PROPERTY_FROM = "homepage.mail.from";

	@Autowired
	private Properties systemProperties;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private VelocityEngine velocityEngine;

	@JmsListener(destination = "HomepageMailQueue")
	public void onMessage(AnswerNotification notification) throws Exception {

		log.debug("New mail notification to send: target {}", notification.getTargetEmail());

		Map<String, Object> model = new HashMap<>();
		model.put("notification", notification);

		MimeMessagePreparator mimeMessagePreparator = new NotificationPreparator(
				velocityEngine,
				model,
				"Answer to your comment at bananamilkshake.me",
				systemProperties.getProperty(PROPERTY_FROM),
				notification.getTargetEmail(),
				"velocity/mail-notification-answer.vm");

		try {
			javaMailSender.send(mimeMessagePreparator);
		} catch (MailException exception) {
			log.error("Exception on mail send", exception);
			throw new Exception("Exception on mail send", exception);
		}
	}
}

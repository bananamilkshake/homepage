# README #

## What is this repository for? ##

This is my simple homepage. You can check it out in [bananamilkshake.me](https://bananamilkshake.me/). It can contains description about me, articles, projects descriptions. It also have an admin page where I can edit all this stuff.

## How do I get set up? ##

### Summary of set up ###
For application server deployment create war file using ``gradle war`` command.
To deploy project directly to Wildfly application server task ``gradle deploy`` can be used.

### Database configuration ###

Project uses [liquibase](http://www.liquibase.org/) to perform database migrations. To create database scheme run this from project directory:

```
#!bat

liquibase --driver="org.postgresql.Driver" \
     --classpath="<path to posgres jdbc driver (https://jdbc.postgresql.org)>" \
     --changeLogFile="database/db.changelog-master.xml" \
     --url="<connection url>" \
     --username='<username to database>' \
     --password='<password to database>' \
     migrate
```

### JSM configuration ###

In order to send notification e-mails it is necessary to configure JMS. Appllication requires JMS queue with name ``HomepageMailQueue`` (jndi value ``java:/jms/HomepageMailQueue``) and JSM connection factory with jndi ``java:/jms/HomepageConnectionFactory``.

### System properties ###

```
#!

homepage.information.id					-- record id in 'information' table to use for information data
homepage.information.cacheExpiration	-- minutes to wait until cached information data is expired
homepage.jms.connectionsCacheSize		-- jms session cache size ()
homepage.mail.connectionTimeout			-- value for smtp mail.smtp.connectiontimeout parameter
homepage.mail.from						-- email to send notification messages
homepage.mail.username					-- login for mail
homepage.mail.password					-- mailbox password
homepage.mail.host						-- mail provider out host
homepage.mail.port						-- mail provider out port
```

## Author ##

If you have any questions about project feel free to write me ``me@bananamilkshake.me``.